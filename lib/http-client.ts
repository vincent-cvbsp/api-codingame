import axios, { AxiosRequestConfig } from 'axios';
import https from 'https';

const httpsAgent =  new https.Agent({
    rejectUnauthorized: false
});
export const HttpClient = (options: AxiosRequestConfig): Promise<any> => {
    if (options) {
        options.httpsAgent = httpsAgent;
    } else {
        options = { httpsAgent };
    }
    return axios(options);
};
