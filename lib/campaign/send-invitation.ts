export class SendInvitation {
    /** name of the candidate */
    candidate_name?: string;
    /** required - email of the candidate */
    candidate_email: string;
     /** email of the recruiter who will receive the test result */
    recruiter_email?: string;
    /** whether send an invitation email to the candidate or not - default true */
    send_invitation_email?: boolean;
    /** whether to send an email to the recruiter when the candidate's invitation bounces (for example, when the candidate's email is rejected by the server) or not - default false */
    send_notification_email_on_bounce?: boolean;
}
