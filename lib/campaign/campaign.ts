export class CgCampaign {
    id: number;
    name: string;
    archived: boolean;
    pinned: boolean;
}
