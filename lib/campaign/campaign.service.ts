import { config } from '@vlemaitre/cvbsp-directory';
import { HttpClient } from '../http-client';
import { CgCampaign } from './campaign';
import { AxiosRequestConfig } from 'axios';
import { SendInvitation } from './send-invitation';
import { SendInvitResponse } from './send-invit-response';

export class CgCampaignService {
    getCampaigns(): Promise<CgCampaign[]> {
        return new Promise<CgCampaign[]>((resolve, reject) => {
            // https://www.codingame.com/assessment/api/v1.1/campaigns
            const options = {
                method: 'GET',
                url: `https://www.codingame.com/assessment/api/v1.1/campaigns`,
                headers: {
                    'API-Key': `${config.CODINGAME.apiKey}`,
                }
            } as AxiosRequestConfig;
            HttpClient(options).then((response) => {
                if (response.status === 200) {
                    return resolve(response.data as CgCampaign[]);
                } else {
                    return reject(response);
                }
            }).catch((error) => {
                return reject(error);
            });
        });
    }

    sendAnInvitation(campaignId: string, data: SendInvitation) {
        return new Promise<SendInvitResponse>((resolve, reject) => {
            // https://www.codingame.com/assessment/api/v1.1/campaigns/{campaign_id}/actions/send
            const options = {
                method: 'POST',
                url: `https://www.codingame.com/assessment/api/v1.1/campaigns/${campaignId}/actions/send`,
                headers: {
                    'API-Key': `${config.CODINGAME.apiKey}`,
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            } as AxiosRequestConfig;
            HttpClient(options).then((response) => {
                if (response.status === 200 && response.data) {
                    return resolve(response.data);
                } else {
                    return reject(response);
                }
            }).catch((error) => {
                return reject(error);
            });
        });
    }

}
