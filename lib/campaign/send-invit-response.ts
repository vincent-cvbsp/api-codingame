export class SendInvitResponse {
    id: number;
    test_url: string;
}
